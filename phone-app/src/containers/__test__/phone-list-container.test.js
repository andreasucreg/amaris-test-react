import React from 'react';
import PhoneListRow from '../../components/PhoneListRow';
import renderer from 'react-test-renderer';
import { Route } from 'react-router';

test('Phone list renders a loader when loading state is true', () => {
    const loading = true;
    const phones = [
        {
            title: 'iPhone 7',
            image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/e7/92/13/1282791/1539-1.jpg',
            description: 'Procesador  Chip A10 Fusion de 64 bits. Tamaño Pantalla  Pantalla Retina HD panorámica de 4,7 pulgadas (en diagonal). Sistema Operativo  iOS 10',
            color: 'white',
            price: '639'
        },
        {
            title: 'Samsung Galaxy S7',
            image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/26/38/12/1194022/1539-1.jpg',
            description: 'Procesador  Octa Core Exynos 8890 ( Quad Core 2,3 GHz + Quad Core 1,6 GHz). Tamaño Pantalla  5,5" / 13,9 cm Corning Gorilla Glass 5 Super AMOLED capacitiva. Sistema Operativo  Android OS, v6.0 Marshmallow',
            color: 'black',
            price: '429'
        },
        {
            title: 'Samsung Galaxy A5',
            image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/ee/0b/14/1313774/1539-1.jpg',
            description: 'Tipo de producto	Smartphone 4G. Procesador	1.9 GHz (8 núcleos). Sistemas operativos	Android 6.0.1 (Marshmallow) Ctd. de ranuras de tarjeta SIM	SIM sencilla',
            color: 'black',
            price: '230'
        },
    ];
    const component = renderer.create(
        loading ?
            <i className="loader fa fa-spinner fa-spin fa-4x"></i>
            :
            <ul>
                {
                    (phones).map((phone, index) =>
                        <li  key={index}>
                            <img className="phone-image" src="phone.image" alt="" />
                            <div className="phone-list-div">
                                <div className="phone-list-title"> Title </div>
                                <div className="phone-list-price"> <strong> Price: </strong> phone.price &euro; </div>
                            </div>
                            <div className="phone-list-options">
                                <div className="phone-list-more"> More info! </div>
                            </div>

                        </li>
                    )
                }
            </ul>
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();



    tree = component.toJSON();

    expect(tree).toMatchSnapshot();


    tree.props.loading = true;
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('Phone list renders a list when loading state is false', () => {
    const loading = false;
    const phones = [
        {
            title: 'iPhone 7',
            image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/e7/92/13/1282791/1539-1.jpg',
            description: 'Procesador  Chip A10 Fusion de 64 bits. Tamaño Pantalla  Pantalla Retina HD panorámica de 4,7 pulgadas (en diagonal). Sistema Operativo  iOS 10',
            color: 'white',
            price: '639'
        },
        {
            title: 'Samsung Galaxy S7',
            image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/26/38/12/1194022/1539-1.jpg',
            description: 'Procesador  Octa Core Exynos 8890 ( Quad Core 2,3 GHz + Quad Core 1,6 GHz). Tamaño Pantalla  5,5" / 13,9 cm Corning Gorilla Glass 5 Super AMOLED capacitiva. Sistema Operativo  Android OS, v6.0 Marshmallow',
            color: 'black',
            price: '429'
        },
        {
            title: 'Samsung Galaxy A5',
            image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/ee/0b/14/1313774/1539-1.jpg',
            description: 'Tipo de producto	Smartphone 4G. Procesador	1.9 GHz (8 núcleos). Sistemas operativos	Android 6.0.1 (Marshmallow) Ctd. de ranuras de tarjeta SIM	SIM sencilla',
            color: 'black',
            price: '230'
        },
    ];
    const component = renderer.create(
        loading ?
            <i className="loader fa fa-spinner fa-spin fa-4x"></i>
            :
            <ul>
                {
                    (phones).map((phone, index) =>
                        <li key={index}>
                            <img className="phone-image" src="phone.image" alt="" />
                            <div className="phone-list-div">
                                <div className="phone-list-title"> Title </div>
                                <div className="phone-list-price"> <strong> Price: </strong> phone.price &euro; </div>
                            </div>
                            <div className="phone-list-options">
                                <div className="phone-list-more"> More info! </div>
                            </div>

                        </li>
                    )
                }
            </ul>
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();



    tree = component.toJSON();

    expect(tree).toMatchSnapshot();


    tree.props.loading = false;
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as phonesActions from '../store/phones/actions';
import PhoneListRow from '../components/PhoneListRow';

class PhoneListContainer extends Component {

    constructor() {
        super();
        this.state = { loading: true }
    }

    componentDidMount() {
        this.props.fetchPhones();
        this.setState({loading: false});
    }

    render(){
        return (
            this.state.loading ? 
                <i className="loader fa fa-spinner fa-spin fa-4x"></i>               
                :                 
                <ul>
                    {
                        (this.props.phone_list.phones).map((phone, index) =>
                            <PhoneListRow key={index} id={index} phone={phone}></PhoneListRow>
                        )
                    }
                </ul>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    fetchPhones: () => {
        dispatch(phonesActions.fetchPhones());
    }
})

const mapStateToProps = state => ({
    phone_list: state.phones
});

export default connect(mapStateToProps, mapDispatchToProps)(PhoneListContainer);
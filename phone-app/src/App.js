import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import PhoneListContainer from './containers/PhoneListContainer';
import PhoneDetailComponent from './components/PhoneDetailComponent';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import * as reducers from './store/reducers';

const store = createStore(combineReducers(reducers), applyMiddleware(thunk));



class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <header className="App-header">
              Amaris
        </header>
            <h1 className="App-title">
              React test!
        </h1>

            <main>
              <Route exact path="/" component={PhoneListContainer} />
              <Route path="/detail" component={PhoneDetailComponent} />
            </main>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;

import React from 'react';
import { Route } from 'react-router';
import PhoneListRow from '../PhoneListRow';
import renderer from 'react-test-renderer';

test('List row renders an li tag', () => {
    const component = renderer.create(
        <li>
            <img className="phone-image" src="" alt="" />
            <div className="phone-list-div">
                <div className="phone-list-title"> Title </div>
                <div className="phone-list-price"> <strong> Price: </strong> XXX &euro; </div>
            </div>
            <div className="phone-list-options">
                <div className="phone-list-more"> More info! </div>
            </div>
        </li>
    );
    
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    tree = component.toJSON();

    expect(tree).toMatchSnapshot();

    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
})
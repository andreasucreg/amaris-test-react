import React, { Component } from 'react';
import { Route } from 'react-router';


class PhoneDetailComponent extends Component {

    render(){
        const phone = this.props.history.location.state.phone;
        const colorBox = {
            borderRadius: '5px',
            backgroundColor: phone.color,
            height: '15px',
            width: '15px',
            display: 'inline-block',
            borderColor: '#757575',
            borderWidth: '1px',
            borderStyle: 'solid'
        }

        return (
            <div>
                <Route render={({ history}) => (                
                    <div className="phone-detail-back" onClick={() => { history.push({ pathname: '/', state: { phone: '' }}) }} > 
                        <i className="fa fa-arrow-left fa-2x"></i>               
                    </div>                    
                )} />
                <h1 class="phone-detail-title"> { phone.title } </h1>
                <img className="phone-detail-image" src={phone.image} alt={phone.title}/>
                
                <div className="phone-detail-div">
                    <div> <strong>Price:</strong> { phone.price } &euro; </div>
                    <div> <strong>Color:</strong> <span style={colorBox}></span></div>
                    <div><strong>Description: </strong></div>
                    <div className="phone-detail-description"> { phone.description } </div>
                </div>
            </div>
        );
    }
}


export default PhoneDetailComponent;
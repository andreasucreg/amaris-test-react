import React, { Component } from 'react';
import { Route } from 'react-router';

class PhoneListRow extends Component {

    render(){
        const phone = this.props.phone;
 
        return (
            <li>
                <img className="phone-image" src={phone.image} alt={phone.title}/>
                <div className="phone-list-div">
                    <div className="phone-list-title"> { phone.title } </div>
                    <div className="phone-list-price"> <strong> Price: </strong> { phone.price } &euro; </div>
                </div>
                <div className="phone-list-options">             
                    <Route render={({ history}) => (                
                        <div className="phone-list-more" onClick={() => { history.push({ pathname: '/detail', state: { phone: phone }}) }} > More info! </div>                    
                    )} />
                </div>
            </li>
        );
    }
}

export default PhoneListRow;
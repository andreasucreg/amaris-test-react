import * as types from './types';

const initialState = {
  phones: [],
  phone: ''
};

export default function reduce(state = initialState, action = {}) {

  switch (action.type) {
    case types.DASHBOARD_LIST_PHONES:
      return {
        ...state,
        phones: action.data
      };
    case types.DASHBOARD_PHONE_ERROR:
      return {
				...state,
				error: action.error
			};
    default:
      return state;
  }
}
import * as types from './types';
import axios from 'axios';

// Consts and Libs
import { phones, base } from '../../constants/globals';

/**
  * Fetch phone list
  */
export function fetchPhones() {

    let url = base + phones;
    
    return (dispatch) => {
        axios.get(url)
        .then(res => {          
            dispatch({ type: types.DASHBOARD_LIST_PHONES, data: res.data });
        })
        .catch (err => {

            dispatch({ type: types.DASHBOARD_PHONE_ERROR, error: 'Could not fetch data' });
        });
    }
}

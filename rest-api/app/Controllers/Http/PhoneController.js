'use strict'

const Route = use('Route')

const phones = [
  {
    title: 'iPhone 7',
    image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/e7/92/13/1282791/1539-1.jpg',
    description: 'Procesador  Chip A10 Fusion de 64 bits. Tamaño Pantalla  Pantalla Retina HD panorámica de 4,7 pulgadas (en diagonal). Sistema Operativo  iOS 10',
    color: 'white',
    price: '639'
  },
  {
    title: 'Samsung Galaxy S7',
    image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/26/38/12/1194022/1539-1.jpg',
    description: 'Procesador  Octa Core Exynos 8890 ( Quad Core 2,3 GHz + Quad Core 1,6 GHz). Tamaño Pantalla  5,5" / 13,9 cm Corning Gorilla Glass 5 Super AMOLED capacitiva. Sistema Operativo  Android OS, v6.0 Marshmallow',
    color: 'black',
    price: '429' 
  },
  {
    title: 'Samsung Galaxy A5',
    image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/ee/0b/14/1313774/1539-1.jpg',
    description: 'Tipo de producto	Smartphone 4G. Procesador	1.9 GHz (8 núcleos). Sistemas operativos	Android 6.0.1 (Marshmallow) Ctd. de ranuras de tarjeta SIM	SIM sencilla',
    color: 'black',
    price: '230' 
  },
  {
    title: 'Moto G5S',
    image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/05/2a/15/1387013/1540-1.jpg',
    description: 'Soporte SIM	Nano-SIM. Tipo de producto	Smartphone. Procesador	Qualcomm MSM8937 Snapdragon 430, Octa-core 1.4 GHz Cortex-A53, Adreno 505 hasta 450 MHz. Sistemas operativos	Android 7.1, Nougat',
    color: 'white',
    price: '230' 
  },
  {
    title: 'Huawei Y6 2017',
    image: 'https://static.fnac-static.com/multimedia/Images/ES/NR/48/c3/14/1360712/1540-1.jpg',
    description: 'Soporte SIM	Micro SIM. Tipo de producto	Smartphone 4G. Procesador	MediaTek MT6737T / 1.4 GHz (núcleo cuádruple) 64 bits. Sistemas operativos	Android 6.0 (Marshmallow) con HUAWEI Emotion UI 4.1',
    color: 'white',
    price: '230' 
  },
];


class PhoneController {

    async index ({response}) {
        return response.json(phones)
    }

    async show ({params, response}) {
      const phone = phones[params.position]
      return response.json(phone)
    }
}

module.exports = PhoneController

# Amaris React Test

This repository contains the react test of Amaris. 
The test consists of two applications:
  a) Adonis application, the REST API with the service to fetch phones data.
  b) React application, the client-side app for displaying the phones data and their details.

NOTE: The applications must be running simultaneously!

# Adonis application instructions

To run this application we first open our terminal in the rest-api folder. Then, check if adonis is installed in your computer.
If not then use the following command:

<code>npm i -g @adonisjs/cli</code>

Then, install dependencies: 
 
<code>npm install</code>

After this run the application with: 

<code>adonis serve --dev</code>

For more information visit: https://www.adonisjs.com/docs/4.1/installation

# React application instructions

To run this application we first open our second terminal in the phone-app folder. Then, check if react is installed in your computer.
If not then use the following command:

<code>npm install react --save</code>

Check if the base url in the constants/globals.js file is the same as the one running for your REST API.

Then, install dependencies: 
 
<code>npm install</code>

After this run the application with: 

<code>npm start</code>

For more information visit: https://reactjs.org/

For running tests copy the following command in your console:

<code>npm test</code>